package it.uniroma3.siw.spring.controller.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import it.uniroma3.siw.spring.model.Credentials;

@Component
public class NewCredentialsValidator extends CredentialsValidator {
	
	@Override
	public void validate(Object o, Errors errors) {
		
		Credentials credentials = (Credentials) o;
		String username = credentials.getUsername().trim();
		String password = credentials.getPassword().trim();
		
		validateUsername(errors, username);
		validatePassword(errors, password);		
	}

	public void validatePassword(Errors errors, String password) {
		if(password.isEmpty())
			errors.rejectValue("password", "required");
		else if(password.length() < MIN_PASSWORD_LENGTH || password.length() > MAX_PASSWORD_LENGTH)
			errors.rejectValue("password", "size");
	}

	public void validateUsername(Errors errors, String username) {
		if(username.isEmpty())
			errors.rejectValue("username", "required");
		else if(username.length() < MIN_USERNAME_LENGTH || username.length() > MAX_USERNAME_LENGTH)
			errors.rejectValue("username", "size");
		if(this.credentialsService.getCredentials(username) != null)
			errors.rejectValue("username", "duplicate");
	}
	
}
