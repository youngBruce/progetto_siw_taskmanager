package it.uniroma3.siw.spring.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.spring.model.Project;
import it.uniroma3.siw.spring.model.Task;
import it.uniroma3.siw.spring.model.User;
import it.uniroma3.siw.spring.repository.ProjectRepository;


@Service
public class ProjectService {
	
	@Autowired
	protected ProjectRepository projectRepository;
		
	@Transactional
	public Project getProject(long id) {
		Optional<Project> result = this.projectRepository.findById(id);
		return result.orElse(null);
	}
	
	@Transactional
	public Project saveProject(Project project) {
		return this.projectRepository.save(project);
	}
	
	@Transactional
	public void deleteProject(Project project) {
		this.projectRepository.delete(project);
	}
	
	@Transactional
	public Project shareProjectWithUser(Project project, User user) {
		project.addMember(user);
		user.addProjects(project);
		return this.projectRepository.save(project);
	}
	
	@Transactional
	public Project unsharedProjectWithUser(Project project, User user) {
		project.getMembers().remove(user);
		user.getVisibleProjects().remove(project);
		return this.projectRepository.save(project);
	}
	
	@Transactional
	public List<Project> retrieveProjectsOwnedBy(User user){
		List<Project> result = new ArrayList<>();
		Iterable<Project> iterable = this.projectRepository.findByOwner(user);
		for(Project project : iterable)
			result.add(project);
		return result;
	}
	
	@Transactional
	public Project addTask(Project project, Task task) {
		project.addTask(task);
		return this.projectRepository.save(project);
	}
	
	@Transactional
	public boolean isShared(Project project, User user) {
		if(project.getMembers().contains(user)) {
			return true;
		}
		return false;
	}

	

}
