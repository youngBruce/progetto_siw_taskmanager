package it.uniroma3.siw.spring.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.spring.controller.session.SessionData;
import it.uniroma3.siw.spring.model.Comment;
import it.uniroma3.siw.spring.model.Project;
import it.uniroma3.siw.spring.model.Tag;
import it.uniroma3.siw.spring.model.Task;
import it.uniroma3.siw.spring.model.User;
import it.uniroma3.siw.spring.service.ProjectService;
import it.uniroma3.siw.spring.service.TagService;
import it.uniroma3.siw.spring.service.TaskService;
import it.uniroma3.siw.spring.service.UserService;

@Controller
public class TaskController {

	@Autowired
	TaskService taskService;

	@Autowired
	SessionData sessionData;

	@Autowired
	ProjectService projectService;

	@Autowired
	UserService userService;

	@Autowired
	TagService tagService;

	private Task currentTask;

	/*CU: aggiungi un task al progetto */
	@RequestMapping(value = {"/projects/{projectId}/addTask"}, method = RequestMethod.GET)
	public String createTaskForm(Model model,  @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		User loggedUser = sessionData.getLoggedUser();
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("project", project);
		model.addAttribute("taskForm", new  Task());
		return "addTask";
	}

	@RequestMapping(value = {"/projects/{projectId}/addTask"}, method = RequestMethod.POST)
	public String createTask(@Valid @ModelAttribute("taskForm") Task task, Model model, @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		User loggedUser = sessionData.getLoggedUser();
		task.setProject(project);
		this.taskService.saveTask(task);
		this.projectService.addTask(project, task);
		model.addAttribute("loggedUser", loggedUser);
		return "redirect:/projects/{projectId}";
	}
	/*CU: aggiungi un task al progetto */

	/*CU: cancella un task dal progetto */
	@RequestMapping(value = {"/projects/{projectId}/{taskId}/delete"}, method = RequestMethod.POST)
	public String removeTask(Model model, @PathVariable Long projectId, @PathVariable Long taskId) {
		Project project = projectService.getProject(projectId);
		Task task = taskService.getTask(taskId);
		this.taskService.deleteTask(task);
		model.addAttribute("project", project);
		return "redirect:/projects/{projectId}";
	}
	/*CU: cancella un task dal progetto */
	
	/*CU: visualizza un task del progetto */
	@RequestMapping(value = {"/projects/{projectId}/task/{taskId}"}, method = RequestMethod.GET)
	public String task(Model model, @PathVariable Long taskId, @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		Task task = taskService.getTask(taskId);
		if(task == null)
			return "redirect:/projects";
		User user = task.getUser();
		List<Tag> tags = task.getTags();
		model.addAttribute("project", project);
		model.addAttribute("loggedUser", sessionData.getLoggedUser());
		model.addAttribute("task", task);
		model.addAttribute("user", user);
		model.addAttribute("tags", tags);
		model.addAttribute("comments", task.getComments());
		model.addAttribute("comment", new Comment());
		return "task";		
	}
	/*CU: visualizza un task del progetto */

    /* visulizza la lista di utenti che hanno visibilità del progetto */
	@RequestMapping(value = {"/projects/{projectId}/task/{taskId}/users"}, method = RequestMethod.GET)
	public String usersTaskList(Model model, @PathVariable Long taskId, @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		Task task = taskService.getTask(taskId);
		User loggedUser = sessionData.getLoggedUser();
		List<User> allUsers = task.getProject().getMembers();
		model.addAttribute("project", project);
		model.addAttribute("task", task);
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("usersList", allUsers);
		return "allUsersTask";
	}

	/*CU: assegna un task con un utente che ha visibilità del progetto*/
	@RequestMapping(value = {"/projects/{projectId}/task/{taskId}/{userId}/share"}, method = RequestMethod.POST)
	public String shareTask(Model model, @PathVariable Long taskId,  @PathVariable Long userId) {
		Task task = taskService.getTask(taskId);
		User loggedUser = sessionData.getLoggedUser();
		User user = userService.getUser(userId);
		List<User> allUsers = task.getProject().getMembers();
		
		//controlla che il task non sia già assegnato
		if(task.getUser()==null) {
			this.taskService.shareTaskWithUser(task, user);
		}
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("task", task);
		model.addAttribute("usersList", allUsers);
		return "redirect:/projects/{projectId}/task/{taskId}";
	}
	/*CU: condivide un task con un utente che ha visibilità del progetto*/

	/* rimuove l'assegnazione di un task da un utente che ha visibilità del progetto*/
	@RequestMapping(value = {"/projects/{projectId}/task/{taskId}/{userId}/unshare"}, method = RequestMethod.POST)
	public String unshareTask(Model model, @PathVariable Long taskId,  @PathVariable Long userId) {
		Task task = taskService.getTask(taskId);
		User user = userService.getUser(userId);
		User loggedUser = sessionData.getLoggedUser();
		List<User> allUsers = task.getProject().getMembers();
		
		//controlla che l'utente sia assegnato al task
		if(task.getUser()!=null && task.getUser()==user) {
			task.setUser(null);
			this.taskService.saveTask(task);			
		}
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("task", task);
		model.addAttribute("usersList", allUsers);
		return "redirect:/projects/{projectId}/task/{taskId}";
	}

	/*CU: aggiorna il task */
	@RequestMapping(value = {"/projects/{projectId}/task/{taskId}/edit"}, method = RequestMethod.GET)
	public String showUpdateTaskForm(Model model, @PathVariable Long taskId, @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		User loggedUser = sessionData.getLoggedUser();
		model.addAttribute("project", project);
		model.addAttribute("loggedUser", loggedUser);
		this.currentTask = taskService.getTask(taskId);	
		model.addAttribute("taskUpdateForm", currentTask);
		return "updateTask";

	}

	@RequestMapping(value = {"/projects/{projectId}/task/{taskId}/edit"}, method = RequestMethod.POST)
	public String updateTaskForm(@Valid @ModelAttribute("taskUpdateForm") Task task, Model model, @PathVariable Long projectId) {
		currentTask.setName(task.getName());
		currentTask.setDescription(task.getDescription());
		User loggedUser = sessionData.getLoggedUser();
		model.addAttribute("loggedUser", loggedUser);
		taskService.saveTask(currentTask);	
		return "redirect:/projects/{projectId}/task/{taskId}";
	}
	/*CU: aggiorna il task */


	/* visualizza la lista dei tag associati al progetto */
	@RequestMapping(value = {"/projects/{projectId}/task/{taskId}/tags"}, method = RequestMethod.GET)
	public String tagsTaskList(Model model, @PathVariable Long taskId, @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		Task task = taskService.getTask(taskId);
		List<Tag> allTags = task.getProject().getTags();
		model.addAttribute("project", project);
		model.addAttribute("task", task);
		model.addAttribute("tagsList", allTags);
		return "allTagsTask";
	}

	/*CU: aggiungere un tag del progetto ad un task del progetto */
	@RequestMapping(value = {"/projects/{projectId}/task/{taskId}/{tagId}/add"}, method = RequestMethod.POST)
	public String addTagToTask(Model model, @PathVariable Long taskId, @PathVariable Long tagId, @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		Task task = taskService.getTask(taskId);
		User loggedUser = sessionData.getLoggedUser();
		Tag tag = tagService.getTag(tagId);
		task.addTag(tag);
		taskService.saveTask(task);
		model.addAttribute("task", task);
		model.addAttribute("project", project);
		model.addAttribute("loggedUser", loggedUser);
		return "redirect:/projects/{projectId}/task/{taskId}";
	}
	/*CU: aggiungere un tag del progetto ad un task del progetto */

	/* rimuove un tag dal task */
	@RequestMapping(value = {"/projects/{projectId}/task/{taskId}/{tagId}/remove"}, method = RequestMethod.POST)
	public String removeTagToTask(Model model, @PathVariable Long taskId, @PathVariable Long tagId, @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		Task task = taskService.getTask(taskId);
		User loggedUser = sessionData.getLoggedUser();
		Tag tag = tagService.getTag(tagId);
		task.getTags().remove(tag);
		taskService.saveTask(task);
		model.addAttribute("task", task);
		model.addAttribute("project", project);
		model.addAttribute("loggedUser", loggedUser);
		return "redirect:/projects/{projectId}/task/{taskId}";
	}

	/* Aggiungere un Commento ad un Task di un progetto su cui ho visibilità (estensione) */
	@RequestMapping(value = { "/projects/{projectId}/task/{taskId}/addComment" }, method = RequestMethod.POST)
	public String postComment( Model model,
							   @PathVariable Long taskId,
							   @ModelAttribute("comment") Comment comment,
							   @PathVariable Long projectId ) {
		if(!comment.getText().isEmpty()) {
			Task currentTask = this.taskService.getTask(taskId);
			comment.setRelativeTask(currentTask);
			comment.setAuthor(sessionData.getLoggedUser());
			currentTask.addComment(comment);
			this.taskService.saveTask(currentTask);
		}
		return "redirect:/projects/{projectId}/task/{taskId}";
	}
	/* Aggiungere un Commento ad un Task di un progetto su cui ho visibilità (estensione) */
}
